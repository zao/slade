#!/usr/bin/env bash

if (type clang++ >/dev/null 2>&1); then
	CC=clang
	CXX=clang++
	CXXFLAGS+=" -stdlib=libc++"
else
	CC=gcc
	CXX=g++
fi

CXXFLAGS+="
-std=c++11"

CXXFLAGS+=" -O3"

CPPFLAGS="
 -Isrc
 -I${HOME}/opt/clang-3.4-libc++/SFML-git/include
 -I${HOME}/lap/SFML/git/include"

LDFLAGS="
 -L${HOME}/opt/clang-3.4-libc++/SFML-git/lib
 -L${HOME}/lap/SFML/git/lib
 -Wl,-rpath
 -Wl,${HOME}/opt/clang-3.4-libc++/SFML-git/lib
 -Wl,-rpath
 -Wl,${HOME}/lap/SFML/git/lib"

slade_LIBS="
 -lsfml-audio
 -lsfml-graphics
 -lsfml-system
 -lsfml-window
 -lrt
 -lassimp
 -lGLEW
 -lGLU
 -lGL"

# -lsfml-network

$CXX \
  ${CXXFLAGS} \
  ${CPPFLAGS} \
  ${LDFLAGS} \
 -o slade \
 src/*.cc \
 src/*.cpp \
 ${slade_LIBS}

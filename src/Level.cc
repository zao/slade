#include "Level.h"
#include "Common.h"

#include <json/json.h>

#include <fstream>
#include <random>

LevelBlueprint
loadLevel(std::string SourceFilename) {
  Json::Value V;
  Json::Reader Reader;
  std::ifstream In(SourceFilename);
  Reader.parse(In, V, false);

  LevelBlueprint Ret;
  auto JsonGifts = V["gifts"];
  for (Json::ArrayIndex i = 0, n = JsonGifts.size(); i < n; ++i) {
    auto JsonGift = JsonGifts[i];
    sf::Vector2f Position;
    Position.x = JsonGift[0].asFloat();
    Position.y = JsonGift[1].asFloat();
    Ret.GiftSpawnPoints.push_back(Position);
  }
  return Ret;
}

Level assembleLevel(LevelBlueprint Blueprint) {
  Level Ret;

  std::mt19937 rng(12345);
  std::uniform_real_distribution<float> InitScaleDist(0.9f, 1.1f);
  std::uniform_real_distribution<float> InitAngleDist(0.0f, 3.14f);
  std::uniform_real_distribution<float> AngularVelocityDist(-2.0f, 2.0f);
  std::uniform_real_distribution<float> ScalePhaseDist(0.0f, 3.14f);
  std::uniform_real_distribution<float> ScaleFreqDist(0.9f, 1.5f);
  std::uniform_real_distribution<float> ScaleBaseDist(0.7f, 0.9f);
  std::uniform_real_distribution<float> ScaleScaleDist(0.1f, 0.2f);
  std::uniform_int_distribution<int> GiftKindDist(0, 1);

  for (auto& Pos : Blueprint.GiftSpawnPoints) {
    GiftRep Rep;
    sf::Vector2f P(Pos.x / (DESIGN_WIDTH/DESIGN_HEIGHT), (0.5f-Pos.y));
    P.x *= DESIGN_WIDTH;
    P.y *= DESIGN_HEIGHT;
    Rep.Position = P;
    Rep.Angle = InitAngleDist(rng);
    Rep.AngularVelocity = AngularVelocityDist(rng);
    Rep.ScaleBase = ScaleBaseDist(rng);
    Rep.ScaleFreq = ScaleFreqDist(rng);
    Rep.ScalePhase = ScalePhaseDist(rng);
    Rep.ScaleScale = ScaleScaleDist(rng);
    Rep.Kind = GiftKindDist(rng);
    Ret.GiftReps.push_back(Rep);
  }
  std::sort(Ret.GiftReps.begin(), Ret.GiftReps.end(), [](GiftRep a, GiftRep b) {
    auto A = a.Position;
    auto B = b.Position;
    if (A.x != B.x)
      return A.x < B.x;
    return A.y < B.y;
  });
  return Ret;
}
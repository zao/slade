#include "Common.h"
#include "Input.h"
#include "Level.h"
#include "MathOps.h"
#include "MathTypes.h"
#include "Mesh.h"
#include "OpenGL.h"
#include "Starfield.h"

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <array>
#include <deque>
#include <iostream>
#include <memory>
#include <random>
#include <sstream>
#include <vector>

struct InfoCollection
{
  struct InfoSink
  {
    std::shared_ptr<std::ostringstream> OSS;
    std::shared_ptr<std::function<void (std::string)>> F;

    template <typename Sink>
    InfoSink(Sink F)
    : F(std::make_shared<std::function<void (std::string)>>(F))
    , OSS(std::make_shared<std::ostringstream>())
    {}

    ~InfoSink() {
      (*F)(OSS->str());
    }

    template <typename T>
    InfoSink const& operator << (T& Value) const {
      *OSS << Value;
      return *this;
    }
  };

  std::map<std::string, std::string> Values;
  InfoSink record(std::string Key) {
    return InfoSink([&](std::string Value)
    {
      record(Key, Value);
    });
  }

  void record(std::string Key, std::string Value) {
    Values[Key] = Value;
  }

  std::string get(std::string Key) {
    return Values[Key];
  }
};

struct GameState {
  GameState() : Points(0) {}
  sf::Vector2f SledPosition;
  ::Level Level;
  int Points;
};

void
duplicatePositions(sf::Vector2f Pos, std::vector<sf::Vector2f>& Out,
  sf::Vector2f StarSkirt) {
  Out.clear();
#if 0
  if (Pos.x < StarSkirt.x || Pos.x > (DESIGN_WIDTH - StarSkirt.x) ||
    Pos.y < StarSkirt.y || Pos.y > (DESIGN_HEIGHT - StarSkirt.y)) {
    for (int X = -1; X <= +1; ++X) {
      for (int Y = -1; Y <= +1; ++Y) {
        Out.emplace_back(Pos.x + X*DESIGN_WIDTH, Pos.y +
          Y*DESIGN_HEIGHT);
      }
    }
  }
  else {
#endif
    Out.emplace_back(Pos);
#if 0
  }
#endif
}

struct Options {
  bool Fullscreen = false;
  bool Mute = false;
};

Options
getOptions(int ArgC, char** ArgV) {
  ::Options Options;
#if 0
  int Option = 0;
  while (true) {
    Option = getopt(ArgC, ArgV, "fm");
    if (Option == -1) break;
    std::cerr << "Option " << Option << "(" << (char)Option << ")\n";
    switch (Option) {
    case 'f': Options.Fullscreen = true; break;
    case 'm': Options.Mute = true; break;
    default: break;
    }
  }
#endif
  return Options;
}



int
main(int ArgC, char** ArgV) {
  auto Options = getOptions(ArgC, ArgV);
  Options.Fullscreen = false;
  Options.Mute = true;
  auto VideoMode = Options.Fullscreen ?
    sf::VideoMode::getDesktopMode() :
    sf::VideoMode(1024, 640, 32);
  auto WindowStyle = Options.Fullscreen ?
    sf::Style::Fullscreen :
    sf::Style::Default;
  sf::ContextSettings ContextSettings;
  ContextSettings.depthBits = 32;
  sf::RenderWindow Window(VideoMode, "Klappjakten", WindowStyle, ContextSettings);
  Window.setVerticalSyncEnabled(true);
  Window.setMouseCursorVisible(false);
  Window.setKeyRepeatEnabled(false);

  sf::Vector2f const Gravity = { 0.0f, -9.82f };

  glewInit();

  sf::SoundBuffer PickupSoundBuffer;
  PickupSoundBuffer.loadFromFile("sound/pickup1.wav");
  sf::Sound PickupSound;
  PickupSound.setBuffer(PickupSoundBuffer);

  sf::Music BGM;
  BGM.openFromFile("sound/Pixel Bells.ogg");
  BGM.setLoop(true);

  LevelBlueprint Blueprint = loadLevel("maps/level1.json");

  GameState State;
  State.Level = assembleLevel(Blueprint);

  Mesh GiftMesh = loadMesh("meshes", "ChristmasBox1.DAE");
  MeshInstance GiftMeshInstance;
  GiftMeshInstance.setMesh(&GiftMesh);
  GiftMeshInstance.setScale(1.0f / 2.0f);

  Mesh SledMesh = loadMesh("meshes", "Sleigh.DAE");
  MeshInstance SledMeshInstance;
  SledMeshInstance.setMesh(&SledMesh);
  SledMeshInstance.setScale(1.0f / 40.0f);

  sf::Transform BoardTransform;
  BoardTransform.scale(VideoMode.width / DESIGN_WIDTH,
    VideoMode.height / DESIGN_HEIGHT);

  sf::Font UIFont;
  UIFont.loadFromFile("art/impact.ttf");

  sf::Text ScoreText;
  ScoreText.setFont(UIFont);
  ScoreText.setCharacterSize(30);
  ScoreText.setString("Points: " + std::to_string(State.Points));
  ScoreText.setPosition(16, 16);

  Starfield BackgroundStarfield;

  sf::FloatRect FieldBounds(-DESIGN_WIDTH / 2.0f, -DESIGN_HEIGHT / 2.0f, DESIGN_WIDTH, DESIGN_HEIGHT);
  sf::Vector2f SledPosition;
  SledPosition = sf::Vector2f(-DESIGN_WIDTH/3.0f, 0.0f);

  std::mt19937 AxisRNG(9001);
  struct Drop {
    sf::Vector2f Position;
    sf::Vector2f Velocity;
    Quaternion Orientation;
    Vec3 SpinAxis;
    float AngularVelocity;
    AABB LocalBounds, GlobalBounds;
  };

  std::vector<Drop> Drops;

  std::vector<AABB> Solids;
  Solids.push_back({{0.0f, -4.0f, 0.0f}, {10.0f, 1.0f, 0.0f}});

  int64_t CurrentFrame = 0;
  int64_t GraphicsFrames = 0;
  int64_t const DropCooldown = (int64_t)(0.5f*(1000.0f/SIM_FRAME_TIME));
  int64_t const DropSlop = (int64_t)(0.1f*(1000.0f/SIM_FRAME_TIME));
  int64_t LastDropFrame = std::numeric_limits<int64_t>::min();
  sf::Clock Clock;
  Clock.restart();
  auto SimTime = Clock.getElapsedTime();
  //auto VisTime = SimTime;

  BidirectionalInput MoveKeyHorizontal;
  BidirectionalInput MoveKeyVertical;
  BidirectionalInput MoveJoyHorizontal;
  BidirectionalInput MoveJoyVertical;
  StackedBidirectionalInput MoveHorizontal = { &MoveKeyHorizontal, &MoveJoyHorizontal };
  StackedBidirectionalInput MoveVertical = { &MoveKeyVertical, &MoveJoyVertical };

  struct ActionEventIntent {
    bool Active = false;
    int64_t ActivationFrame = 0;

    void
    Activate(int64_t FrameRequested) {
      Active = true;
      ActivationFrame = FrameRequested;
    }

    void
    Acknowledge() {
      Active = false;
    }
  };

  ActionEventIntent DropIntent;

  if (!Options.Mute) {
    BGM.play();
  }
  InfoCollection Info;
  int64_t FrameLastSecond = 0;
  sf::Time LastSecond = Clock.getElapsedTime();
  while (true) {
    sf::Event Event;
    while (Window.pollEvent(Event)) {
      switch (Event.type) {
      case sf::Event::Closed:
        Window.close();
        break;
      case sf::Event::KeyPressed:
        switch (Event.key.code) {
        case sf::Keyboard::Escape:
          Window.close();
          break;
        case sf::Keyboard::Up:
          MoveKeyVertical.pushNegative();
          break;
        case sf::Keyboard::Down:
          MoveKeyVertical.pushPositive();
          break;
        case sf::Keyboard::Left:
          MoveKeyHorizontal.pushNegative();
          break;
        case sf::Keyboard::Right:
          MoveKeyHorizontal.pushPositive();
          break;
        case sf::Keyboard::Space:
          DropIntent.Activate(CurrentFrame);
          break;
        default: break;
        }
        break;
      case sf::Event::KeyReleased:
        switch (Event.key.code) {
        case sf::Keyboard::Up:
          MoveKeyVertical.popNegative();
          break;
        case sf::Keyboard::Down:
          MoveKeyVertical.popPositive();
          break;
        case sf::Keyboard::Left:
          MoveKeyHorizontal.popNegative();
          break;
        case sf::Keyboard::Right:
          MoveKeyHorizontal.popPositive();
          break;
        case sf::Keyboard::Space:
          break;
        default: break;
        }
      default: break;
      }
    }
    if (!Window.isOpen())
      break;
    if (sf::Joystick::isConnected(0)) {
      float X = sf::Joystick::getAxisPosition(0, sf::Joystick::X);
      float Y = sf::Joystick::getAxisPosition(0, sf::Joystick::Y);

      if (fabs(X) > JOYSTICK_DEADZONE) {
        if (X < 0.0f) MoveJoyHorizontal.pushNegative();
        if (X > 0.0f) MoveJoyHorizontal.pushPositive();
      }
      else MoveJoyHorizontal.reset();
      if (fabs(Y) > JOYSTICK_DEADZONE) {
        if (Y < 0.0f) MoveJoyVertical.pushNegative();
        if (Y > 0.0f) MoveJoyVertical.pushPositive();
      }
      else MoveJoyVertical.reset();
    }
    else {
      MoveJoyHorizontal.reset();
      MoveJoyVertical.reset();
    }

    auto Now = Clock.getElapsedTime();
    auto Dt = sf::milliseconds(SIM_FRAME_TIME);
    while (SimTime + Dt <= Now) {
      if (DropIntent.Active && DropIntent.ActivationFrame + DropSlop < CurrentFrame) {
        DropIntent.Acknowledge();
      }
      for (auto& Layer : BackgroundStarfield.Layers) {
        auto Velocity = sf::Vector2f(Layer.first, 0.0f);
        for (auto& Star : Layer.second.Stars) {
          auto& Pos = Star.Position;
          Pos += Dt.asSeconds() * Velocity;
          Pos.x = std::fmod(Pos.x + DESIGN_WIDTH, DESIGN_WIDTH);
          Pos.y = std::fmod(Pos.y + DESIGN_HEIGHT, DESIGN_HEIGHT);
          Star.Angle += Dt.asSeconds() * Star.Spin;
        }
      }
      float VelocitiesHorizontal[] = { 0.0f, -6.0f, 6.0f, 0.0f };
      float VelocitiesVertical[] = { 0.0f, 6.0f, -6.0f, 0.0f };
      if (float v = VelocitiesHorizontal[MoveHorizontal.direction()]) {
        SledPosition.x += Dt.asSeconds() * v;
      }
      if (float v = VelocitiesVertical[MoveVertical.direction()]) {
        SledPosition.y += Dt.asSeconds() * v;
      }
      {
        SledPosition.x = clamp(SledPosition.x, FieldBounds.left,
          FieldBounds.left + FieldBounds.width);
        SledPosition.y = clamp(SledPosition.y, FieldBounds.top,
          FieldBounds.top + FieldBounds.height);
      }
      for (auto& Gift : State.Level.GiftReps) {
        Gift.Position.x += Dt.asSeconds() * WORLD_VELOCITY;
        Gift.Angle += Dt.asSeconds() * Gift.AngularVelocity;
        Gift.Scale = Gift.ScaleBase + Gift.ScaleScale * sin(SimTime.asSeconds() * Gift.ScaleFreq + Gift.ScalePhase);
      }
      for (auto& Drop : Drops) {
        Drop.Velocity += Gravity * Dt.asSeconds();
        Drop.Position += Drop.Velocity * Dt.asSeconds();
        auto DeltaO = quaternionFromAxisAngle(Drop.AngularVelocity * Dt.asSeconds(), Drop.SpinAxis);
        Drop.Orientation = DeltaO * Drop.Orientation;
        Drop.GlobalBounds = Drop.LocalBounds;
        Drop.GlobalBounds.C = Drop.GlobalBounds.C + Vec3{Drop.Position.x, Drop.Position.y, 0.0f};
      }
      std::deque<size_t> DestroyDropList;
      for (decltype(Drops.size()) n = Drops.size(), i = 0; i < n; ++i) {
        auto& Drop = Drops[i];
        for (auto& Solid : Solids) {
          if (TestAABBAABB(Drop.GlobalBounds, Solid) == IntersectTestResult::Positive) {
            DestroyDropList.push_front(i);
          }
        }
      }
      for (auto Index : DestroyDropList) {
        Drops.erase(Drops.begin() + Index);
      }
      auto& Gifts = State.Level.GiftReps;
      while (!Gifts.empty() && Gifts.front().Position.x < -0.1f*DESIGN_WIDTH) {
        Gifts.pop_front();
      }
      for (size_t i = 0; i < Gifts.size();) {
        sf::Vector2f Diff = (SledPosition - Gifts[i].Position);
        float D = sqrt(Diff.x*Diff.x + Diff.y*Diff.y);
        if (D < 64.0f) {
          Gifts.erase(Gifts.begin() + i);
          State.Points += 1000;
          ScoreText.setString("Points: " + std::to_string(State.Points));
          if (!Options.Mute) {
            PickupSound.play();
          }
        }
        else
          ++i;
      }

      if (Gifts.empty()) {
        State.Level = assembleLevel(Blueprint);
      }

      auto DropBox = [&](sf::Vector2f Pos) {
        Drop D = {};
        D.Position = Pos;
        D.Velocity = { 5.0f, 0.0f };
        D.Orientation = quaternionIdentity();
        D.SpinAxis = randomAxis(AxisRNG);
        D.AngularVelocity = 3.0f;
        D.LocalBounds = { { 0.0f, 0.0f, 0.0f }, { 0.5f, 0.5f, 0.5f } };
        Drops.push_back(std::move(D));
      };

      if (DropIntent.Active && LastDropFrame + DropCooldown <= CurrentFrame) {
        DropIntent.Acknowledge();
        DropBox(SledPosition);
        LastDropFrame = CurrentFrame;
      }

#define DEBUG_DROP_SPAM 1
#if DEBUG_DROP_SPAM
      if ((CurrentFrame % 1) == 0) {
        static std::uniform_real_distribution<float> XDist(-DESIGN_WIDTH/2.0f, DESIGN_WIDTH/2.0f);
        static std::uniform_real_distribution<float> YDist(-DESIGN_HEIGHT/2.0f, DESIGN_HEIGHT/2.0f);
        auto X = XDist(AxisRNG), Y = YDist(AxisRNG);
        DropBox(sf::Vector2f(X, Y));
      }
#endif

      SimTime += Dt;
      ++CurrentFrame;
    }
    {
      std::vector<sf::Vector2f> TempPositions;
      TempPositions.reserve(9);
      Window.clear(sf::Color(26, 51, 76));
      Window.pushGLStates();
      glClear(GL_DEPTH_BUFFER_BIT);
      glEnable(GL_LIGHTING);
      glEnable(GL_LIGHT0);
      glEnable(GL_LIGHT1);
      glLightfv(GL_LIGHT0, GL_DIFFUSE, from(Vec4{0.4f, 0.4f, 0.2f, 1.0f}));
      glLightfv(GL_LIGHT1, GL_DIFFUSE, from(Vec4{0.2f, 0.2f, 0.4f, 1.0f}));
      Vec4 Light0Pos = { 0.0f, 0.5f, 10.0f, 1.0f };
      Vec4 Light1Pos = { -0.5f, -0.3f, 10.0f, 1.0f };
      glEnable(GL_DEPTH_TEST);
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      gluPerspective(45, 16.0f / 9.0f, 0.1f, 100.0f);
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
      glTranslatef(0.0f, 0.0f, -10.0f);
      glLightfv(GL_LIGHT0, GL_POSITION, (float const*)&Light0Pos);
      glLightfv(GL_LIGHT1, GL_POSITION, (float const*)&Light1Pos);
      sf::Vector3f Pos(SledPosition.x, SledPosition.y, 0.0f);
      if (MoveHorizontal.direction() == BidirectionalInput::NEGATIVE)
        SledMeshInstance.setRotation(15.0f);
      else if (MoveHorizontal.direction() == BidirectionalInput::POSITIVE)
        SledMeshInstance.setRotation(-15.0f);
      else
        SledMeshInstance.setRotation(0.0f);
      SledMeshInstance.setPosition(Pos);
      SledMeshInstance.draw();
      GiftMeshInstance.setPosition({ -SledPosition.x, SledPosition.y, 0.0f });
      GiftMeshInstance.setRotation(0.0f);
      GiftMeshInstance.draw();
      for (auto& Drop : Drops) {
        GiftMeshInstance.setPosition({ Drop.Position.x, Drop.Position.y, 0.0f });
        GiftMeshInstance.setOrientation(Drop.Orientation);
        GiftMeshInstance.draw();
      }
      Window.popGLStates();
#if 0
      for (auto& Layer : BackgroundStarfield.Layers) {
        for (auto& Star : Layer.second.Stars) {
          auto Pos = Star.Position;
          duplicatePositions(Pos, TempPositions, StarSkirt);
          for (auto& VirtualPos : TempPositions) {
            Star.Sprite->setPosition(Pos);
            Star.Sprite->setRotation(Star.Angle * 180.0f / pi<float>());
            Star.Sprite->setColor(Layer.second.Color);
            Window.draw(*Star.Sprite, BoardTransform);
          }
        }
      }
      {
        if (MoveHorizontal.direction() == BidirectionalInput::NEGATIVE)
          SledSprite.setRotation(-15.0f);
        else if (MoveHorizontal.direction() == BidirectionalInput::POSITIVE)
          SledSprite.setRotation(15.0f);
        else
          SledSprite.setRotation(0.0f);
        SledSprite.setPosition(SledPosition);
        Window.draw(SledSprite, BoardTransform);
      }
      for (auto& Gift : State.Level.GiftReps) {
        auto& GiftSprite = GiftSprites[Gift.Kind];
        GiftSprite.setPosition(Gift.Position);
        GiftSprite.setRotation(Gift.Angle * 180.0f / pi<float>());
        GiftSprite.setScale(Gift.Scale, Gift.Scale);
        Window.draw(GiftSprite, BoardTransform);
      }
      Window.draw(ScoreText, BoardTransform);
#endif
      Window.display();
      ++GraphicsFrames;
      if ((Now - LastSecond).asSeconds() >= 1.0) {
        auto Span = Now - LastSecond;
        auto N = GraphicsFrames - FrameLastSecond;
        double FrameRate = N/Span.asSeconds();
        LastSecond = Now;
        FrameLastSecond = GraphicsFrames;
        Info.record("graphics.framerate") << FrameRate;
        std::cerr << "graphics.framerate: " << Info.get("graphics.framerate") << "\n";
      }
    }
  }
}

// vim: set ts=2 sw=2 et cin:

#pragma once

#include "MathTypes.h"

#include <iosfwd>

std::ostream&
operator << (std::ostream& OS, Quaternion Q);
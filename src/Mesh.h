#pragma once

#include "OpenGL.h"
#include <SFML/Graphics.hpp>

#include <memory>
#include <vector>

struct MeshAttribute {
  uint32_t ByteOffset;
  uint32_t Stride;
};

inline uint32_t
byteSize(MeshAttribute const& Attribute, size_t NumEntries) {
  return NumEntries * Attribute.Stride;
}

inline uint32_t
byteBeginOffset(MeshAttribute const& Attribute) {
  return Attribute.ByteOffset;
}

inline uint32_t
byteEndOffset(MeshAttribute const& Attribute, size_t NumEntries) {
  return Attribute.ByteOffset + byteSize(Attribute, NumEntries);
}

struct Mesh {
  ~Mesh() {}

  struct Material {
    std::shared_ptr<sf::Texture> TextureImage;
  };
  std::vector<Material> Materials;

  struct SubMesh {
    std::shared_ptr<GLBuffer<OwnedId>> VBId;
    std::shared_ptr<GLBuffer<OwnedId>> IBId;
    uint8_t MaterialIndex;
    uint32_t NumIndices;
    uint32_t NumVertices;
    MeshAttribute PositionAttribute;
    MeshAttribute NormalAttribute;
    MeshAttribute TexcoordAttribute;
  };
  std::vector<SubMesh> SubMeshes;
};

Mesh
loadMesh(char const* BasePath, char const* Filename);

struct MeshInstance {
  void
  setMesh(Mesh* NewMesh);

  Mesh*
  getMesh() const;

  void
  draw() const;

  sf::Vector3f
  getPosition() const;
  
  sf::Vector3f
  getOrigin() const;

  float
  getScale() const;

  void
  setPosition(sf::Vector3f NewPosition);

  void
  setOrigin(sf::Vector3f NewOrigin);

  void
  setRotation(float NewAngle);

  void
  setOrientation(Quaternion Orientation);

  void
  setScale(float NewScale);

private:
  Mesh* CurrentMesh = nullptr;
  sf::Vector3f Position = sf::Vector3f(0.0f, 0.0f, 0.0f);
  sf::Vector3f Origin = sf::Vector3f(0.0f, 0.0f, 0.0f);
  Quaternion Orientation = quaternionIdentity();
  float Scale = 1.0f;
};
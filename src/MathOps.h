#pragma once

#include "Common.h"
#include "MathTypes.h"
#include <cmath>
#include <random>

inline float
min(float A, float B) {
  return (A<B) ? A : B;
}

inline float
max(float A, float B) {
  return (A<B) ? B : A;
}

inline float
clamp(float X, float A, float B) {
  return min(max(X, A), B);
}

inline Vec3
operator + (Vec3 V1, Vec3 V2) {
  return {
    V1.X + V2.X,
    V1.Y + V2.Y,
    V1.Z + V2.Z
  };
}

inline Vec3
operator - (Vec3 V1, Vec3 V2) {
  return {
    V1.X - V2.X,
    V1.Y - V2.Y,
    V1.Z - V2.Z
  };
}

inline Vec3
operator - (Vec3 V) {
  return {
    -V.X,
    -V.Y,
    -V.Z
  };
}

inline Vec3
operator * (Vec3 V, float K) {
  return {
    K*V.X,
    K*V.Y,
    K*V.Z
  };
}

inline Vec3
operator * (float K, Vec3 V) {
  return {
    K*V.X,
    K*V.Y,
    K*V.Z
  };
}

inline float
dot(Vec3 V1, Vec3 V2) {
  return V1.X*V2.X + V1.Y*V2.Y + V1.Z*V2.Z;
}

inline Vec3
cross(Vec3 V1, Vec3 V2) {
  return {
    V1.Y*V2.Z - V1.Z*V2.Y,
    V1.Z*V2.X - V1.X*V2.Z,
    V1.X*V2.Y - V1.Y*V2.X
  };
}

inline float
norm(Vec3 V) {
  return sqrt(dot(V, V));
}

inline Quaternion
operator + (Quaternion Q1, Quaternion Q2) {
  return {
    Q1.R + Q2.R,
    Q1.V + Q2.V
  };
}

inline Quaternion
operator * (Quaternion Q1, Quaternion Q2) {
  return {
    Q1.R*Q2.R - dot(Q1.V, Q2.V),
    Q1.R*Q2.V + Q2.R*Q1.V + cross(Q1.V, Q2.V)
  };
}

inline Quaternion
conjugate(Quaternion Q) {
  return {
    Q.R,
    -Q.V
  };
}

inline float
norm(Quaternion Q) {
  return sqrtf(Q.R*Q.R + dot(Q.V, Q.V));
}

inline Quaternion
quaternionFromAxisAngle(float Angle, Vec3 Axis) {
  return {
    cosf(Angle/2.0f),
    sinf(Angle/2.0f)*Axis
  };
}

inline Quaternion
quaternionIdentity() {
  return {
    1.0f,
    { -.0f, 0.0f, 0.0f }
  };
}

inline Mat4
matrixFromQuaternion(Quaternion Q) {
  auto A = Q.R, B = Q.V.X, C = Q.V.Y, D = Q.V.Z;
  /* | aa + bb - cc - dd | 2bc - 2ad         | 2bd + 2ac         |
     | 2bc + 2ad         | aa - bb + cc - dd | 2cd - 2ab         |
     | 2bd - 2ac         | 2cd + 2ab         | aa - bb - cc + dd |
  */
  Mat4 Ret = { // column vectors
    Vec4{ A*A + B*B - C*C - D*D, 2*B*C + 2*A*D, 2*B*D - 2*A*C, 0.0f },
    Vec4{ 2*B*C - 2*A*D, A*A - B*B + C*C - D*D, 2*C*D + 2*A*B, 0.0f },
    Vec4{ 2*B*D + 2*A*C, 2*C*D - 2*A*B, A*A - B*B - C*C + D*D, 0.0f },
    Vec4{ 0.0f, 0.0f, 0.0f, 1.0f }
  };
  return Ret;
}

inline Mat4
matrixIdentity() {
  Mat4 Ret = {};
  Ret(0, 0) = Ret(1, 1) = Ret(2, 2) = Ret(3, 3) = 1.0f;
  return Ret;
}

inline float
radToDeg(float A) {
  return A * 180.0f / pi<float>();
}

inline float
degToRad(float A) {
  return A * pi<float>() / 180.0f;
}

template <typename Generator>
Vec3
randomAxis(Generator& RNG) {
  std::uniform_real_distribution<float> AngleDist(0.0f, 2*pi<float>());
  std::uniform_real_distribution<float> ZDist(-1.0f, 1.0f);
  float Theta = AngleDist(RNG);
  float Z = ZDist(RNG);
  return {
    sqrtf(1-Z*Z)*cosf(Theta),
    sqrtf(1-Z*Z)*sinf(Theta),
    Z
  };
}

inline IntersectTestResult
TestAABBAABB(AABB A, AABB B) {
  if (fabs(A.C[0] - B.C[0]) > (A.R[0] + B.R[0]))
    return IntersectTestResult::Negative;
  if (fabs(A.C[1] - B.C[1]) > (A.R[1] + B.R[1]))
    return IntersectTestResult::Negative;
  if (fabs(A.C[2] - B.C[2]) > (A.R[2] + B.R[2]))
    return IntersectTestResult::Negative;
  return IntersectTestResult::Positive;
}

/* RTCD p103-105 */
inline IntersectTestResult
TestOBBOBB(OBB A, OBB B) {
  float const EPSILON = 0.0001f;
  float RA, RB;
  Mat3 R, AbsR;

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      R(i, j) = dot(A.U[i], B.U[i]);
    }
  }

  Vec3 T = B.C - A.C;
  T = { dot(T, A.U[0]), dot(T, A.U[1]), dot(T, A.U[2]) };

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      AbsR(i, j) = fabs(R(i, j)) + EPSILON;
    }
  }

  // Test axes L = A0, L = A1, L = A2
  for (int i = 0; i < 3; ++i) {
    RA = A.E[i];
    RB = B.E[0] * AbsR(i, 0) + B.E[1] * AbsR(i, 1) + B.E[2] * AbsR(i, 2);
    if (fabs(T[i]) > RA + RB)
      return IntersectTestResult::Negative;
  }

  // Test axes L = B0, L = B1, L = B2
  for (int i = 0; i < 3; ++i) {
    RA = A.E[0] * AbsR(0, i) + A.E[1] * AbsR(1, i) + A.E[2] * AbsR(2, i);
    RB = B.E[i];
    if (fabs(T[0] * R(0, i) + T[1] * R(1, i) + T[2] * R(2, i)) > RA + RB)
      return IntersectTestResult::Negative;
  }

  // Test axis L = A0 x B0
  RA = A.E[1] * AbsR(2, 0) + A.E[2] * AbsR(1, 0);
  RB = B.E[1] * AbsR(0, 2) + B.E[2] * AbsR(0, 1);
  if (fabs(T[2] * R(1, 0) - T[1] * R(2, 0)) > RA + RB)
    return IntersectTestResult::Negative;

  // Test axis L = A0 x B1
  RA = A.E[1] * AbsR(2, 1) + A.E[2] * AbsR(1, 1);
  RB = B.E[0] * AbsR(0, 2) + B.E[2] * AbsR(0, 0);
  if (fabs(T[2] * R(1, 1) - T[1] * R(2, 1)) > RA + RB)
    return IntersectTestResult::Negative;

  // Test axis L = A0 x B2
  RA = A.E[1] * AbsR(2, 2) + A.E[2] * AbsR(1, 2);
  RB = B.E[0] * AbsR(0, 1) + B.E[1] * AbsR(0, 0);
  if (fabs(T[2] * R(1, 2) - T[1] * R(2, 2)) > RA + RB)
    return IntersectTestResult::Negative;

  // Test axis L = A1 x B0
  RA = A.E[0] * AbsR(2, 0) + A.E[2] * AbsR(0, 0);
  RB = B.E[1] * AbsR(1, 2) + B.E[2] * AbsR(1, 1);
  if (fabs(T[0] * R(2, 0) - T[2] * R(0, 0)) > RA + RB)
    return IntersectTestResult::Negative;

  // Test axis L = A1 x B1
  RA = A.E[0] * AbsR(2, 1) + A.E[2] * AbsR(0, 1);
  RB = B.E[0] * AbsR(1, 2) + B.E[2] * AbsR(1, 0);
  if (fabs(T[0] * R(2, 1) - T[2] * R(0, 1)) > RA + RB)
    return IntersectTestResult::Negative;

  // Test axis L = A1 x B2
  RA = A.E[0] * AbsR(2, 2) + A.E[2] * AbsR(0, 2);
  RB = A.E[0] * AbsR(1, 1) + B.E[1] * AbsR(1, 0);
  if (fabs(T[0] * R(2, 2) - T[2] * R(0, 2)) > RA + RB)
    return IntersectTestResult::Negative;

  // Test axis L = A2 x B0
  RA = A.E[0] * AbsR(1, 0) + A.E[1] * AbsR(0, 0);
  RB = B.E[1] * AbsR(2, 2) + B.E[2] * AbsR(2, 1);
  if (fabs(T[1] * R(0, 0) - T[0] * R(1, 0)) > RA + RB)
    return IntersectTestResult::Negative;

  // Test axis L = A2 x B1
  RA = A.E[0] * AbsR(1, 1) + A.E[1] * AbsR(0, 1);
  RB = B.E[0] * AbsR(2, 2) + B.E[2] * AbsR(2, 0);
  if (fabs(T[1] * R(0, 1) - T[0] * R(1, 1)) > RA + RB)
    return IntersectTestResult::Negative;

  // Test axis L = A2 x B2
  RA = A.E[0] * AbsR(1, 2) + A.E[1] * AbsR(0, 2);
  RB = B.E[0] * AbsR(2, 1) + B.E[1] * AbsR(2, 0);
  if (fabs(T[1] * R(0, 2) - T[0] * R(1, 2)) > RA + RB)
    return IntersectTestResult::Negative;

  return IntersectTestResult::Positive;
}

inline IntersectTestResult
TestBoundingSphereBoundingSphere(BoundingSphere A, BoundingSphere B) {
  Vec3 D = A.C - B.C;
  float Dist2 = dot(D, D);
  float RadiusSum = A.R + B.R;
  return (Dist2 < RadiusSum * RadiusSum)
    ? IntersectTestResult::Positive
    : IntersectTestResult::Negative;
}

inline AABB
AABBFromBoundingSphere(BoundingSphere S) {
  AABB Ret = {
    S.C,
    { S.R, S.R, S.R }
  };
  return Ret;
}

inline BoundingSphere
BoundingSphereFromAABB(AABB B) {
  BoundingSphere Ret = {
    B.C,
    max(B.R[0], max(B.R[1], B.R[2]))
  };
  return Ret;
}

inline float const*
from(Vec3 const& V) {
    return (float const*)&V;
}

inline float const*
from(Vec4 const& V) {
    return (float const*)&V;
}

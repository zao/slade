#pragma once

#include <SFML/System.hpp>

#include <deque>
#include <vector>

struct GiftRep {
  sf::Vector2f Position;
  float Angle;
  float Scale;
  float AngularVelocity;
  float ScalePhase;
  float ScaleFreq;
  float ScaleScale;
  float ScaleBase;
  int Kind;
};

struct LevelBlueprint {
  std::vector<sf::Vector2f> GiftSpawnPoints;
};

struct Level {
  std::deque<GiftRep> GiftReps;
};

LevelBlueprint
loadLevel(std::string SourceFilename);

Level
assembleLevel(LevelBlueprint Blueprint);
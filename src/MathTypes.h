#pragma once

#include <cassert>
#include <cstddef>

struct Vec3 {
  float X, Y, Z;

  float&
  operator () (size_t Index) {
    assert(Index <= 2);
    if (Index == 0) return X;
    if (Index == 1) return Y;
    return Z;
  }

  float
  operator () (size_t Index) const {
    assert(Index <= 2);
    if (Index == 0) return X;
    if (Index == 1) return Y;
    return Z;
  }

  float&
  operator [] (size_t Index) {
    return (*this)(Index);
  }

  float
  operator [] (size_t Index) const {
    return (*this)(Index);
  }
};

struct Quaternion {
  float R;
  Vec3 V;
};

struct Vec4 {
  float X, Y, Z, W;

  float&
  operator () (size_t Index) {
    assert(Index <= 3);
    if (Index == 0) return X;
    if (Index == 1) return Y;
    if (Index == 2) return Z;
    return W;
  }

  float
  operator () (size_t Index) const {
    assert(Index <= 3);
    if (Index == 0) return X;
    if (Index == 1) return Y;
    if (Index == 2) return Z;
    return W;
  }

  float&
  operator [] (size_t Index) {
    return (*this)(Index);
  }

  float
  operator [] (size_t Index) const {
    return (*this)(Index);
  }
};

struct Mat3 {
  Vec3 Col[3];

  Vec3&
  operator () (size_t Column) {
    assert(Column <= 2);
    return Col[Column];
  }

  Vec3
  operator () (size_t Column) const {
    assert(Column <= 2);
    return Col[Column];
  }

  float&
  operator () (size_t Row, size_t Column) {
    return (*this)(Column)(Row);
  }

  float
  operator () (size_t Row, size_t Column) const {
    return (*this)(Column)(Row);
  }
};

struct Mat4 {
  Vec4 Col[4];

  Vec4&
  operator () (size_t Column) {
    assert(Column <= 3);
    return Col[Column];
  }

  Vec4
  operator () (size_t Column) const {
    assert(Column <= 3);
    return Col[Column];
  }

  float&
  operator () (size_t Row, size_t Column) {
    return (*this)(Column)(Row);
  }

  float
  operator () (size_t Row, size_t Column) const {
    return (*this)(Column)(Row);
  }
};

struct AABB {
  Vec3 C;
  Vec3 R;
};

struct OBB {
  Vec3 C;    // center-point
  Vec3 U[3]; // local axes
  Vec3 E;    // positive halfwidth extents
};

struct BoundingSphere {
  Vec3 C;
  float R;
};

enum class IntersectTestResult {
  Negative = 0,
  Positive = 1
};

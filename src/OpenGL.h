#pragma once

#include <GL/glew.h>
#include <SFML/OpenGL.hpp>

#include <memory>
#include <type_traits>

struct OwnedId;
struct BorrowedId;

template <typename OwningPolicy>
struct GLBuffer {
  GLBuffer() = default;
  ~GLBuffer() {
    if (std::is_same<OwningPolicy, OwnedId>::value) {
      glDeleteBuffers(1, &Id);
    }
  }

  unsigned Id = 0;
private:
  GLBuffer(GLBuffer const&) = delete;
  GLBuffer& operator = (GLBuffer const&) = delete;
};

inline GLuint*
into(GLuint& Id) {
  return &Id;
}

template <typename OwningPolicy>
GLuint*
into(GLBuffer<OwningPolicy>& Buffer) {
  return &Buffer.Id;
}

template <typename OwningPolicy>
inline GLuint*
into(std::shared_ptr<GLBuffer<OwningPolicy>>& BufferPointer) {
  return &BufferPointer->Id;
}

inline GLuint
from(GLuint Id) {
  return Id;
}

template <typename OwningPolicy>
GLuint
from(GLBuffer<OwningPolicy> const& Buffer) {
  return Buffer.Id;
}

template <typename OwningPolicy>
GLuint
from(std::shared_ptr<GLBuffer<OwningPolicy>> const& BufferPointer) {
  return BufferPointer->Id;
}
#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <map>
#include <memory>
#include <vector>

struct Star {
  sf::Vector2f Position;
  float Angle;
  float Spin;
  std::shared_ptr<sf::Sprite> Sprite;
};

struct StarLayer {
  sf::Color Color;
  std::vector<Star> Stars;
};

struct Starfield {
  Starfield();

  std::vector<std::string> Sources;
  std::vector<std::shared_ptr<sf::Texture>> Textures;
  std::map<float, StarLayer> Layers;
};
#include "MathIO.h"

#include <cmath>
#include <iostream>

struct PosNegAppend {
  PosNegAppend(float A) : A(A) {}
  float A;

  template <typename CharT, typename TraitsT>
  inline friend std::basic_ostream<CharT, TraitsT>&
  operator << (std::basic_ostream<CharT, TraitsT>& OS, PosNegAppend const& PNA) {
    if (PNA.A < 0.0f) return OS << "- " << std::fabs(PNA.A);
    return OS << "+ " << PNA.A;
  }
};

std::ostream&
operator << (std::ostream& OS, Quaternion Q) {
  OS << "(" << Q.R << " ";
  OS << PosNegAppend(Q.V.X) << "i ";
  OS << PosNegAppend(Q.V.Y) << "j ";
  OS << PosNegAppend(Q.V.Z) << "k)";
  return OS;
}

#include "Starfield.h"
#include "Common.h"

#include <random>

Starfield::Starfield() {
  Sources.push_back("art/star1.png");

  for (auto& Source : Sources) {
    auto Tex = std::make_shared<sf::Texture>();
    Tex->loadFromFile(Source);
    Tex->setSmooth(true);
    Textures.push_back(Tex);
  }

  {
    float const Speeds[] = { -200.0f, -150.0f, -100.0f };
    float const Scales[] = { 1.0f, 0.7f, 0.5f };
    sf::Color Colors[] = {
      sf::Color(180, 180, 180, 255),
      sf::Color(100, 100, 100, 255),
      sf::Color(80, 80, 80, 255)
    };
    std::mt19937 rng(12345);
    std::uniform_real_distribution<float> XDist(0.0f, DESIGN_WIDTH);
    std::uniform_real_distribution<float> YDist(0.0f, DESIGN_HEIGHT);
    std::uniform_real_distribution<float> AngleDist(0.0f, 3.14f);
    std::uniform_real_distribution<float> SpinDist(-2.0f, 2.0f);
    for (int i = 0; i < 3; ++i) {
      auto Speed = Speeds[i];
      Layers[Speed].Color = Colors[i];
      auto& Stars = Layers[Speed].Stars;
      for (int k = 0; k < 50; ++k) {
        auto X = XDist(rng);
        auto Y = YDist(rng);
        auto Angle = AngleDist(rng);
        auto Spin = SpinDist(rng);
        Star S = {sf::Vector2f(X, Y), Angle, Spin, std::make_shared<sf::Sprite>()};
        S.Sprite->setTexture(*Textures[0]);
        auto Bounds = S.Sprite->getLocalBounds();
        S.Sprite->setOrigin(Bounds.width/2.0f, Bounds.height/2.0f);
        S.Sprite->setScale(Scales[i], Scales[i]);
        Stars.push_back(std::move(S));
      }
    }
  }
}

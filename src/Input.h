#pragma once

#include <vector>

struct BidirectionalInput {
  enum Direction : int {
    NONE = 0, NEGATIVE = 1, POSITIVE = 2, BOTH = 3
  };

  void pushNegative() { (int&)Dir |= NEGATIVE; }
  void pushPositive() { (int&)Dir |= POSITIVE; }
  void popNegative() { (int&)Dir &= ~NEGATIVE; }
  void popPositive() { (int&)Dir &= ~POSITIVE; }

  void reset() { Dir = NONE; }

  Direction direction() const {
    return Dir;
  }

private:
  Direction Dir = NONE;
};

struct StackedBidirectionalInput {
  typedef BidirectionalInput::Direction Direction;
  std::vector<BidirectionalInput const*> Inputs;

  StackedBidirectionalInput() {}
  StackedBidirectionalInput(std::initializer_list<BidirectionalInput const*> Inputs)
    : Inputs(Inputs)
  {}

  Direction direction() const {
    Direction Dir = Direction::NONE;
    for (auto Input : Inputs) {
      (int&)Dir = Input->direction() | Dir;
    }
    return Dir;
  }
};
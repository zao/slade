#include "MathOps.h"
#include "MathIO.h"
#include "Mesh.h"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <array>
#include <cstdint>
#include <cstring>
#include <sstream>
#include <vector>

static std::shared_ptr<sf::Texture>
loadTexture(std::string FullPath) {
  auto Tex = std::make_shared<sf::Texture>();
  Tex->loadFromFile(FullPath);
  Tex->setSmooth(true);
  return Tex;
}

Mesh
loadMesh(char const* BasePath, char const* Filename) {
  Mesh Ret;
  Assimp::Importer Importer;
  std::string FullPath = std::string(BasePath) + "/" + Filename;
  auto ProcessFlags =
    aiProcess_RemoveRedundantMaterials |
    aiProcess_Triangulate |
    aiProcess_PreTransformVertices |
    aiProcess_JoinIdenticalVertices |
    aiProcess_TransformUVCoords |
    aiProcess_FlipUVs |
    aiProcess_GenSmoothNormals |
    0;
  aiScene const* Scene = Importer.ReadFile(FullPath, ProcessFlags);

  // ===[Load materials]===
  for (unsigned i = 0; i < Scene->mNumMaterials; ++i) {
    auto SourceMat = Scene->mMaterials[i];
    Mesh::Material Mat;

    aiString Path;
    int NumTextures = SourceMat->GetTextureCount(aiTextureType_DIFFUSE);
    if (NumTextures != 0) {
      if (aiReturn_SUCCESS == SourceMat->GetTexture(aiTextureType_DIFFUSE, 0, &Path)) {
        Mat.TextureImage = loadTexture(std::string(BasePath) + "/" + Path.C_Str());
      }
    }
    Ret.Materials.push_back(Mat);
  }

  // ===[Load meshes into source arrays]===
  for (unsigned i = 0; i < (false ? 1 : Scene->mNumMeshes); ++i) {
    auto SourceMesh = Scene->mMeshes[i];
    Mesh::SubMesh Sub;
    Sub.MaterialIndex = SourceMesh->mMaterialIndex;

    auto VB = std::make_shared<GLBuffer<OwnedId>>();
    Sub.NumVertices = SourceMesh->mNumVertices;
    std::vector<char> VertexData;
    {
      enum { NUM_ATTRIBUTES = 3 };
      auto N = SourceMesh->mNumVertices;

#if 0
      N = 4;
      std::array<float, 3> Points[] = { { -0.5f, -0.5f, 0.0f }, { 0.5f, -0.5f, 0.0f }, { 0.5f, 0.5f, 0.0f }, { -0.5f, 0.5f, 0.0f } };
      std::array<float, 2> Texcoords[] = { { 0.0f, 1.0f }, { 1.0f, 1.0f }, { 1.0f, 0.0f }, { 0.0f, 0.0f } };

      std::array<float, 3> Normal = { 0.0f, 0.0f, -1.0f }, Normals[] = { Normal, Normal, Normal, Normal };
#endif

      Sub.PositionAttribute.ByteOffset = 0;
      Sub.PositionAttribute.Stride = sizeof(aiVector3D);
      Sub.NormalAttribute.ByteOffset = byteEndOffset(Sub.PositionAttribute, N);
      Sub.NormalAttribute.Stride = sizeof(aiVector3D);
      Sub.TexcoordAttribute.ByteOffset = byteEndOffset(Sub.NormalAttribute, N);
      Sub.TexcoordAttribute.Stride = sizeof(aiVector2D);

      VertexData.resize(byteEndOffset(Sub.TexcoordAttribute, N));
      char* P = VertexData.data();
#if 0
      std::memcpy(P + byteBeginOffset(Ret.PositionAttribute), Points,    byteSize(Ret.PositionAttribute, N));
      std::memcpy(P + byteBeginOffset(Ret.NormalAttribute),   Normals,   byteSize(Ret.NormalAttribute, N));
      std::memcpy(P + byteBeginOffset(Ret.TexcoordAttribute), Texcoords, byteSize(Ret.TexcoordAttribute, N));
#else
      std::memcpy(P + byteBeginOffset(Sub.PositionAttribute), SourceMesh->mVertices, byteSize(Sub.PositionAttribute, N));
      std::memcpy(P + byteBeginOffset(Sub.NormalAttribute), SourceMesh->mNormals, byteSize(Sub.NormalAttribute, N));
      {
        aiVector3D const* SrcP = SourceMesh->mTextureCoords[0];
        char* DstP = P + byteBeginOffset(Sub.TexcoordAttribute);
        for (size_t i = 0; i < N; ++i) {
          std::memcpy(DstP, SrcP, sizeof(aiVector2D));
          ++SrcP;
          DstP += sizeof(aiVector2D);
        }
      }
#endif
    }

    // ===[Create OpenGL buffers]===
    glGenBuffers(1, into(VB));
    glBindBuffer(GL_ARRAY_BUFFER, from(VB));
    glBufferData(GL_ARRAY_BUFFER, VertexData.size(), VertexData.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    Sub.VBId = VB;

    auto IB = std::make_shared<GLBuffer<OwnedId>>();
    std::vector<uint32_t> Indices;
    Indices.reserve(SourceMesh->mNumFaces * 3);
    for (unsigned FaceIndex = 0, NumFaces = SourceMesh->mNumFaces; FaceIndex < NumFaces; ++FaceIndex) {
      auto& Face = SourceMesh->mFaces[FaceIndex];
      Indices.insert(Indices.end(), Face.mIndices, Face.mIndices + Face.mNumIndices);
    }
    Sub.NumIndices = Indices.size();

    glGenBuffers(1, into(IB));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, from(IB));
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, Indices.size() * sizeof(uint32_t), Indices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    Sub.IBId = IB;
    Ret.SubMeshes.push_back(Sub);
  }
  return Ret;
}

Mesh*
MeshInstance::getMesh() const {
  return CurrentMesh;
}

void
MeshInstance::setMesh(Mesh* NewMesh) {
  CurrentMesh = NewMesh;
}

sf::Vector3f
MeshInstance::getPosition() const {
  return Position;
}

sf::Vector3f
MeshInstance::getOrigin() const {
  return Origin;
}

float
MeshInstance::getScale() const {
  return Scale;
}

void
MeshInstance::setPosition(sf::Vector3f NewPosition) {
  Position = NewPosition;
}

void
MeshInstance::setOrigin(sf::Vector3f NewOrigin) {
  Origin = NewOrigin;
}

void
MeshInstance::setRotation(float NewRotation) {
  this->Orientation = quaternionFromAxisAngle(degToRad(NewRotation), {0.0f, 0.0f, 1.0f});
}

void
MeshInstance::setOrientation(Quaternion Orientation) {
  this->Orientation = Orientation;
}

void
MeshInstance::setScale(float NewScale) {
  Scale = NewScale;
}

void
MeshInstance::draw() const {
  if (!CurrentMesh) return;

  glEnable(GL_TEXTURE_2D);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glTranslatef(Position.x, Position.y, Position.z);
  {
    Quaternion RotQ = Orientation;
    Mat4 RotM = matrixFromQuaternion(RotQ);
    glMultMatrixf((float const*)&RotM);
  }
  glScalef(Scale, Scale, Scale);
  glTranslatef(-Origin.x, -Origin.y, -Origin.z);

  GLuint VAOId;
  glGenVertexArrays(1, into(VAOId));
  glBindVertexArray(from(VAOId));

  bool WasBlending = glIsEnabled(GL_BLEND) == GL_TRUE;
  glDisable(GL_BLEND);

  for (auto& Sub : CurrentMesh->SubMeshes) {
    auto& Material = CurrentMesh->Materials.at(Sub.MaterialIndex);

    sf::Texture::bind(Material.TextureImage.get());
    glBindBuffer(GL_ARRAY_BUFFER, from(Sub.VBId));
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glTexCoordPointer(2, GL_FLOAT, Sub.TexcoordAttribute.Stride,
      (void*)(uintptr_t)Sub.TexcoordAttribute.ByteOffset);
    glNormalPointer(GL_FLOAT, Sub.NormalAttribute.Stride,
      (void*)(uintptr_t)Sub.NormalAttribute.ByteOffset);
    glVertexPointer(3, GL_FLOAT, Sub.PositionAttribute.Stride,
      (void*)(uintptr_t)Sub.PositionAttribute.ByteOffset);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, from(Sub.IBId));
    glDrawElements(GL_TRIANGLES, Sub.NumIndices, GL_UNSIGNED_INT, nullptr);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glVertexPointer(4, GL_FLOAT, 0, nullptr);
    glNormalPointer(GL_FLOAT, 0, nullptr);
    glTexCoordPointer(4, GL_FLOAT, 0, nullptr);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    sf::Texture::bind(nullptr);
  }

  (WasBlending ? glEnable : glDisable)(GL_BLEND);
  glBindVertexArray(0);
  glDeleteVertexArrays(1, &VAOId);

  glPopMatrix();
}

// vim: set ts=2 sw=2 et:

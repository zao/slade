#pragma once

enum {
  SIM_FRAME_TIME = 8,
};

template <typename T> T pi() { return (T)3.14159265358979323846264338327950L; }

static float const DESIGN_WIDTH = 16.0f;
static float const DESIGN_HEIGHT = 9.0f;
static float const WORLD_VELOCITY = -5.0f;
static float const JOYSTICK_DEADZONE = 20.0f;